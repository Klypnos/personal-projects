#include<cstdio>
#include<math.h>
bool isPrime(int num)
{
    int i=2;
    bool isPrime=1;
    if(num<2) return 0;
    if(num==2) return 1;
    for(i;i<=sqrt(num);i++)
    {
        if(num%i==0)
        {
            isPrime=0;
            break;
        }
    }
    return isPrime;
}
int howManyPrime(int a, int b)
{
    int counter=0;
    int n=0;
    int hold;
    while(1)
    {
        hold=n*n+a*n+b;
        n++;
        if(isPrime(hold))
        {
            counter++;
            continue;
        }
        return counter;
    }
}

int main(int argc, char *argv[])
{
    int a=0,b=0,c=0;
    int max[2]={0};
    int maxvalue=-1;
    for(b=2;b<1000;b++)
    {
        if(isPrime(b)==1)
        for(a=-999;a<1000;a++)
        {
            if( isPrime(a+b+1)==1)
            {
                if( ( c=howManyPrime(a,b) ) >maxvalue)
                {
                    max[0]=a;
                    max[1]=b;
                    maxvalue=c;
                    printf(" %d %d %d\n",a,b,c);
                }
            }

        }
    }
    printf("a=%d b=%d multiplication=%d\n",max[0],max[1],max[0]*max[1]);
    return 0;
}
