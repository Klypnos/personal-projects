#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
typedef enum { false = 0, true = !false } bool;
typedef struct BSTElement_{ // branches
char SerieName[50];
int  Watched;
int  Exist;
int factor;
struct BSTElement_ *right;
struct BSTElement_ *left;
} BSTElement;

typedef struct BST_{ //root
BSTElement *root;
} BST;
// functions of BST
/*check*/void BST_Init(BST *tree);
/*check*/bool BST_Insert(BST *tree,char* name,int watch,int exist);
/*check*/bool BST_Delete(BST *tree,char *name);
/*check*/void BST_Close(BSTElement *root);
/*check*/BSTElement* BST_Traverse(BST *tree,char* name);
/*check*/bool BST_Update(BST *tree, char* name, int watch, int exist); //update watched episodes
/*check*/void BST_Save(BSTElement *node,FILE *fptr); // if desired save to file
/*check*/void BST_Make(BST *tree, FILE *fptr); //read from file and make BST
/*check*/void BST_List(BSTElement *node);
/*check*/int CalcFactor(BST* tree,BSTElement *node);
/*check*/int IsBalanced(BSTElement *node);
int BST_Balancer(BST* tree);
// functions of BST end
// functions and contents
void BST_Init(BST *tree)
{
    tree->root=NULL;
}
void BST_Close(BSTElement *root)
{
    BSTElement* traverse=root;
    if(traverse)
    {
        BST_Close(traverse->left);
        BST_Close(traverse->right);
        free(traverse);
    }
}
int CalcFactor(BST* tree,BSTElement *node)
{
    int countleft=0;
    int countright=0;
     BSTElement *traversel=NULL;
     BSTElement *traverser=NULL;
    if((tree->root)->left)
    {
        traversel=tree->root->left;
        countleft++;
    }
    if((tree->root)->right)
    {
        traverser=tree->root->right;
        countright++;
    }

    if(traversel)
    {
         while( traversel->left || traversel->right)
        {
            if(traversel->left)
            {
                traversel=traversel->left;
                countleft++;
            }
            else if(traversel->right)
            {
                traversel=traversel->right;
                countleft++;
            }
        }
    }
    if(traverser)
    {
         while( traverser->left || traverser->right)
        {
            if(traverser->left)
            {
                traverser=traverser->left;
                countright++;
            }
            else if(traverser->right)
            {
                traverser=traverser->right;
                countright++;
            }
        }
    }
   return countleft-countright;
}

int IsBalanced(BSTElement *node)
{
    BSTElement *traverse=node;
    if(traverse)
    {
        if(traverse->factor>1 || traverse->factor< -1)
            return 0;
        else
            {

            if( !(IsBalanced(traverse->left)) )
                return 0;
            if( !(IsBalanced(traverse->right)) )
                return 0;
            return 1;
            }
    }
    else
            return 1;
}

bool BST_Insert(BST *tree,char* name,int watch,int exist)
{
    BSTElement *traverse=tree->root;
    if(!traverse) //first node
    {
        traverse=(BSTElement*) malloc(sizeof(BSTElement));
        strcpy(traverse->SerieName,name);
        traverse->Watched=watch;
        traverse->Exist=exist;
        traverse->left=NULL;
        traverse->right=NULL;
        tree->root=traverse;
        traverse->factor=CalcFactor(tree,traverse);
        return 1;
    }
    else
    {
        while(strcmp(traverse->SerieName,name) != 0)
        {
            if(traverse->right && strcmp(traverse->SerieName,name)< 0)
            {
                traverse=traverse->right;
                continue;
            }
            else if(!traverse->right && strcmp(traverse->SerieName,name) < 0)
            {
                traverse->right=(BSTElement*) malloc(sizeof(BSTElement));
                traverse=traverse->right;
                strcpy(traverse->SerieName,name);
                traverse->Watched=watch;
                traverse->Exist=exist;
                traverse->left=NULL;
                traverse->right=NULL;
                traverse->factor=CalcFactor(tree,traverse);
                return 1;
            }
            if(traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                traverse=traverse->left;
                continue;
            }
            else if(!traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                traverse->left=(BSTElement*) malloc(sizeof(BSTElement));
                traverse=traverse->left;
                strcpy(traverse->SerieName,name);
                traverse->Watched=watch;
                traverse->Exist=exist;
                traverse->left=NULL;
                traverse->right=NULL;
                traverse->factor=CalcFactor(tree,traverse);
                return 1;
            }
        }
        printf("This serie has already been added\n");
        return 0;
    }
}

void BST_List(BSTElement *node)
{
    BSTElement *traverse=node;
    if(traverse)
    {
        BST_List(traverse->left);
        printf("%20s ",traverse->SerieName);
        printf("%5d ",traverse->Watched);
        //printf("%5d ",traverse->factor);
        printf("%5d\n",traverse->Exist);
        BST_List(traverse->right);
    }
}
// functions and contents end
BSTElement* BST_Traverse(BST *tree,char* name)
{
    BSTElement *traverse=tree->root;
    while(strcmp(traverse->SerieName,name) != 0)
    {
         if(traverse->right && strcmp(traverse->SerieName,name)< 0)
            {
                traverse=traverse->right;
                continue;
            }
            else if(!traverse->right && strcmp(traverse->SerieName,name)< 0)
            {
               printf("No such record exists.\n");
               return NULL;
            }
            if(traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                traverse=traverse->left;
                continue;
            }
            else if(!traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                printf("No such record exists.\n");
                    return NULL;
            }
    }
    return traverse;
}

bool BST_Update(BST *tree,char* name,int watch, int exist)
{
    BSTElement *traverse=tree->root;
    if( (traverse=BST_Traverse(tree,name)) == NULL)
    {
        printf("No such serie in the list.\n");
        return 0;
    }
    else //found
    {
        traverse->Watched=watch;
        traverse->Exist=exist;
        return 1;
    }
}

bool BST_Delete(BST *tree,char *name)
{
    BSTElement *tail,*traverse,*tail2;
    tail=traverse=tail2=tree->root;
    bool direction; // 0 left 1 right
     while(strcmp(traverse->SerieName,name) != 0)
    {
         if(traverse->right && strcmp(traverse->SerieName,name)< 0)
            {
                tail=traverse;
                traverse=traverse->right;
                direction=1;
                continue;
            }
            else if(!traverse->right && strcmp(traverse->SerieName,name)< 0)
            {
               printf("No such record exists.\n");
               return 0;
            }
            if(traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                tail=traverse;
                traverse=traverse->left;
                direction=0;
                continue;
            }
            else if(!traverse->left && strcmp(traverse->SerieName,name)> 0)
            {
                printf("No such record exists.\n");
                return 0;
            }
    }
    //no child // 0 left 1 right
    if(traverse->left==NULL && traverse->right==NULL)
        {
            if(direction==0)
            {
                tail->left=NULL;
            }
            else if(direction==1)
                tail->right=NULL;
            else
                tree->root=NULL;
        }
    //left child
    else if(traverse->right==NULL)
    {
        if(direction==0)
           {
               tail->left=traverse->left;
           }
        else if(direction==1)
            {
                tail->right=traverse->left;
            }
            else
                tree->root=traverse->left;
    }
    //right child
     else if(traverse->left==NULL)
    {
        if(direction==0)
           {
               tail->left=traverse->right;
           }
        else if(direction==1)
            {
                tail->right=traverse->right;
            }
            else
                tree->root=traverse->right;
    }
    //both children
    else
    {
        tail2=traverse->right;
    while(tail2->left)
        tail2=tail2->left;
        tail2->left=traverse->left;
        if(direction==0)
            tail->left=traverse->right;
        else if(direction==1)
            tail->right=traverse->right;
        else
            tree->root=traverse->right;
    }
    free(traverse);
    return 1;
}

void BST_Save(BSTElement *node,FILE *fptr)
{
   // fseek(fptr,0,SEEK_SET);
    BSTElement *traverse=node;
    if(traverse)
    {
        fprintf(fptr,"%s %d %d\n",traverse->SerieName,traverse->Watched,traverse->Exist);
        BST_Save(traverse->left,fptr);
        BST_Save(traverse->right,fptr);
    }
}

void BST_Make(BST *tree,FILE *fptr)
{
    int count=0;
    char string[100];
    char SerieName[50];
    char *snp=SerieName;
    char watch[5];
    char exist[5];
    char *ptr=string;
    int i=0;
    while(fgets(string,100,fptr))
    {
        i=0;
        count=0;
        ptr=string;
        snp=SerieName;
        string[strlen(string)]='\0';
        while( (*ptr) != '\0')
        {
            if((*ptr)==' ')
                {count++;}
            ptr++;
        }
        ptr=string;
            while(i<count-1)
            {
                *snp=*ptr;
                snp++;
                ptr++;
                if(*(ptr)==' ')
                {
                    i++;
                }
            }
            SerieName[snp-SerieName]='\0';
            ptr++;
            snp=watch;
            while(*ptr!= ' ')
            {
                *snp=*ptr;
                snp++;
                ptr++;
            }
            watch[snp-watch]='\0';
            ptr++;
            snp=exist;
            while((*ptr)!= '\n')
            {
                *snp=*ptr;
                snp++;
                ptr++;
            }
            exist[snp-exist]='\0';
          /*  SerieName[strlen(SerieName)]='\0';
            watch[strlen(watch)]='\0';
            exist[strlen(exist)]='\0';*/
            BST_Insert(tree,SerieName,atoi(watch),atoi(exist));
    }
}

void Use_BST_Insert(BST* tree)
{
    char SerieName[50];
    int exist;
    int watch;
    printf("Enter the name of the serie you want to add\n");
    scanf(" %[^\n]s",SerieName);
    printf("Episodes watched?\n");
    scanf(" %d",&watch);
    printf("Episodes exist?\n");
    scanf(" %d",&exist);
    BST_Insert(tree, SerieName, watch, exist);
}

void Use_BST_Delete(BST *tree)
{
    char SerieName[50];
     printf("Enter the name of the serie you want to delete\n");
     scanf(" %[^\n]s",SerieName);
     BST_Delete(tree, SerieName);
}

void Use_BST_Update(BST* tree)
{
        char SerieName[50];
    int exist;
    int watch;
    printf("Enter the name of the serie you want to update\n");
    scanf(" %[^\n]s",SerieName);
    printf("Updated watched episodes?\n");
    scanf(" %d",&watch);
    printf("Updated existing episodes?\n");
    scanf(" %d",&exist);
    BST_Update(tree,SerieName,watch,exist);
}

int BST_Balancer(BST* tree)
{
}
int main()
{
    FILE *fptr;
    BST record;
    BST_Init(&record);
   char choice;
   bool out=0;
    if( (fptr=(fopen("record.txt","r+"))) )
    {
       BST_Make(&record,fptr);
    }
    else
    {
        if(( fptr=(fopen("record.txt","w") ) ))
            {
                printf("You're using this program for first time or text file is in wrong location.\n"
                       "If this is your first time ignore this message, otherwise put the text file\n"
                       "in same location with record.exe and re-open the program.\n");
            }
            else{
                printf("File could not be opened. Program will be terminated.\n");
                return 0;
            }
    }
    //One of "ifs" worked

    while(!out)
    {
        BST_List(record.root);
        printf("\n  1. Add\n");
        printf("  2. Delete\n");
        printf("  3. Update\n");
        printf("  4. Save\n");
        printf("Choose one of the options <Press any other key to exit>\n");
        scanf(" %c",&choice);
        switch(choice)
        {
        //Add, Delete, Update, Close, Save
    case '1':

        Use_BST_Insert(&record);
        break;
    case '2':
        Use_BST_Delete(&record);
        break;
    case '3':
        Use_BST_Update(&record);
        break;
    case '4':
        fseek(fptr,0,SEEK_SET);
        BST_Save(record.root, fptr);
        break;
    default:
        printf("Are you sure you want to leave? If you did not save, your changes will take no effect.(Y/N)\n");
        scanf(" %c",&choice);
        if( (choice)=='y' || (choice) =='Y')
        {
          out=1;
          fclose(fptr);
          //BST_Close();
        }
        else
            continue;
        break;
        }
    }
    return 0;
}
